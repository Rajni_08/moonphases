﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AssetBundleHandler : MonoBehaviour {

	public string assetBundleUrl;
	AssetBundle assetBundle;

	public void LoadScene(string SceneName){
		assetBundle = AssetBundleDownloader.getAssetBundle (assetBundleUrl +""+ SceneName);
		if(!assetBundle) StartCoroutine (DownloadAB(SceneName));
	}

	IEnumerator DownloadAB (string SceneName){
		yield return StartCoroutine(AssetBundleDownloader.downloadAssetBundle (assetBundleUrl +""+ SceneName));
		assetBundle = AssetBundleDownloader.getAssetBundle (assetBundleUrl +""+ SceneName);
		string[] scenePath = assetBundle.GetAllScenePaths();
		SceneManager.LoadSceneAsync(System.IO.Path.GetFileNameWithoutExtension(scenePath[0]));
	}

}
