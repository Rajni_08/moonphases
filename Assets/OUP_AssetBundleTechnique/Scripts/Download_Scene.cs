﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Download_Scene : MonoBehaviour {

	public string assetBundleUrl;
	AssetBundle assetBundle;
	public Text ProgressText;
	// Use this for initialization
	void Start () {
		assetBundle = AssetBundleDownloader.getAssetBundle (assetBundleUrl);
		if(!assetBundle) StartCoroutine (DownloadAB());
	}

	// Update is called once per frame
	void Update () {
//			if(!Downloader.wwwProgress.isDone){
//				ProgressText.text = "Loading : " + Downloader.wwwProgress.progress * 100;
//			}
	}


	public void LoadScene(){
		assetBundle = AssetBundleDownloader.getAssetBundle (assetBundleUrl);
		if(!assetBundle) StartCoroutine (DownloadAB());
	}

	IEnumerator DownloadAB (){
		yield return StartCoroutine(AssetBundleDownloader.downloadAssetBundle (assetBundleUrl));
		assetBundle = AssetBundleDownloader.getAssetBundle (assetBundleUrl);
		//string[] scenePath = assetBundle.GetAllAssetNames();

		// Testing GameObject
		//-----------------------------------------------------
		AssetBundleRequest request = assetBundle.LoadAssetAsync("AssetBundleGO", typeof(GameObject));
		yield return request;
		GameObject _myPrefab = request.asset as GameObject;
		Instantiate(_myPrefab);
		//-----------------------------------------------------

	//	SceneManager.LoadSceneAsync(System.IO.Path.GetFileNameWithoutExtension(scenePath[0]));
	}
}
