﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadModelOnDetection : MonoBehaviour
{

	public enum SceneType { AR, ThreeD, None };

	#region private variables.
	Coroutine coroutine, loadModelCoroutine;
	Transform model;
	SceneType sceneType;
	bool isModelDetected = false;
	#endregion

	#region delegate define.
	public delegate void OnModelLoaded(string name, Transform _transform, bool status, SceneType sceneType);
	public static event OnModelLoaded isModelLoaded;
	#endregion

	public GameObject[] gameObjects;
	public static LoadModelOnDetection mInstance;

	private void Start()
	{
		if (mInstance == null)
		{
			mInstance = this;
			sceneType = SceneType.None;
		}
		else
		{
			Destroy(gameObject);
		}
	}

	private void OnEnable()
	{
		TrackableEventHandler.onImageTargetDetected += TrackableEventHandler_OnImageTargetDetected;
	}

	private void OnDisable()
	{
		TrackableEventHandler.onImageTargetDetected -= TrackableEventHandler_OnImageTargetDetected;
	}

	#region delegate handler.
	void TrackableEventHandler_OnImageTargetDetected(string arg, Transform _transform, bool isFound)
	{
		Debug.Log("deleggaet called " + isFound);
		isModelDetected = isFound;
		if (isModelDetected)
		{
            loadModelCoroutine = StartCoroutine(LoadModel(arg, _transform));
            loadModelCoroutine = StartCoroutine(LoadModelData(arg, _transform, true));
		}
		else
		{
			if (loadModelCoroutine != null)
				StopCoroutine(loadModelCoroutine);
			if (isModelLoaded != null)
			{
                StopCoroutine("DestroyedChild");
                StartCoroutine(DestroyedChild(_transform));
                isModelLoaded(arg, _transform, false, SceneType.None);
			}
		}

	}
	#endregion

	public IEnumerator LoadModel(string modelName, Transform parent)
	{
		Debug.Log(modelName);
		if (coroutine != null)
			StopCoroutine(coroutine);
		    coroutine = StartCoroutine(LoadAssetBundleData.instance.LoadAssetBundleUsingManifest(modelName, parent, OnLoadComplete));
		yield return coroutine;
	}

	void OnLoadComplete(string modelName, Transform parent, AssetBundle bundle)
	{
		StopCoroutine("DestroyedChild");
		StartCoroutine(DestroyedChild(parent));
		model = bundle.LoadAsset<GameObject>(modelName).transform;
		if (isModelDetected)
			model = Instantiate(model, parent).transform;
		model.name = modelName;

		Debug.Log(model.localPosition.y);

        model.localPosition = model.localPosition;
        model.localScale = model.localScale;
        model.localRotation = model.localRotation;
        bundle.Unload(false);

		if (isModelLoaded != null)
		{
			if (isModelDetected)
				isModelLoaded(modelName, parent, true, SceneType.AR);
			else  // this condition when 3d called
				isModelLoaded(modelName, model.transform, true, SceneType.ThreeD);
		}


	}

	IEnumerator DestroyedChild(Transform child)
	{
		Debug.Log("child count : " + child.childCount);
		if (child.childCount > 0)
		{
			for (int i = 0; i < child.childCount; i++)
			{
				Destroy(child.GetChild(i).gameObject);
			}
		}
		yield return null;
	}

	//
	IEnumerator LoadModelData(string namedata, Transform transform, bool status)
	{
		yield return new WaitForEndOfFrame();
		if (status)
		{
            Debug.Log("staus");
            StopCoroutine("DestroyedChild");
            StartCoroutine(DestroyedChild(transform));

            for (int i = 0; i < gameObjects.Length; i++)
			{
                //Debug.Log(gameObjects[i].name.ToLower());
				if (namedata.ToLower().Contains(gameObjects[i].name.ToLower()))
				{
					model = gameObjects[i].transform;
                    model = Instantiate(model, transform).transform;
                    model.name = gameObjects[i].name;
					isModelLoaded(namedata, transform, true, LoadModelOnDetection.SceneType.AR);
				}
			}
		}
		else
		{
			isModelLoaded(namedata, transform, false, LoadModelOnDetection.SceneType.None);
		}

	}
}