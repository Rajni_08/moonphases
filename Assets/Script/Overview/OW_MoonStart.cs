﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OW_MoonStart : MonoBehaviour {

	// Update is called once per frame
	void Update ()
    {
        if(transform.localScale != new Vector3(0.8f, 0.8f, 0.8f))
        transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);

        transform.Rotate(Vector3.up, 20f* Time.deltaTime);
	}
}
