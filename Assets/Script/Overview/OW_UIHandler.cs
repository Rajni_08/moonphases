﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OW_UIHandler : MonoBehaviour {


    public static OW_UIHandler instance;

    public GameObject info_moon;
    public GameObject overview;
    public GameObject Ui_Panels;
    public GameObject atmosphere_Btn;
    public GameObject temperature_Btn;
    public GameObject timePeriodOfRevolution_Btn;
    public GameObject timePeriodOfRotation_Btn;
    public GameObject lengthofday_Btn;
    public GameObject searching_Icon_Parent;
    public GameObject AppInstruction_Panel;
    public GameObject searching_Icon;
    public GameObject Thermometer;
    public Text MoonName;
    public Text Descripton_Text;


    // Use this for initialization
    void Awake ()
    {
        instance = this;	
	}
	
}
