﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OW_Rotation : MonoBehaviour {

    public Transform target;
    public float OrbitDegrees = 1f;

    void Update()
    {
        if (OverviewScript.instance.path)
            transform.RotateAround(target.transform.position, Vector3.down, OrbitDegrees);
    }
}
