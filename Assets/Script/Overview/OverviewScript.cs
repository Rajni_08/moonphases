﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class OverviewScript : MonoBehaviour {

    public VuforiaUnity.VuforiaHint HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS { get; private set; }

    public static OverviewScript instance;

    public List<string> Description;
    public List<GameObject> LoadedModel;
    public GameObject detectedModel;
    public Material mat;
    internal bool path;
    Material initmat;
 

    // Use this for initialization
    void Start ()
    {
        instance = this;
        VuforiaUnity.SetHint(HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, 1);
    }

    private void OnEnable()
    {
        LoadModelOnDetection.isModelLoaded += OnModelLoaded;
    }

    private void OnDisable()
    {
        LoadModelOnDetection.isModelLoaded -= OnModelLoaded;
    }

    private void OnModelLoaded(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
    {
        MAIP_ZoomInOut_Script.instance.isModlDetected = status;

        if (status)
        {
            detectedModel = _transform.GetChild(0).gameObject;

            if (detectedModel.transform.GetChildCount() > 0)
            {
                for (int i = 0; i < detectedModel.transform.GetChildCount(); i++)
                {
                    LoadedModel.Add(detectedModel.transform.GetChild(i).gameObject);
                }
                initmat = LoadedModel[0].transform.GetChild(0).GetComponent<MeshRenderer>().material;
            }

            MAIP_ZoomInOut_Script.instance.currModel = detectedModel;
            Debug.Log("currModel :" + MAIP_ZoomInOut_Script.instance.currModel);
            OW_UIHandler.instance.Ui_Panels.SetActive(true);
            OW_UIHandler.instance.overview.SetActive(true);// edited
            OW_UIHandler.instance.AppInstruction_Panel.SetActive(false);
            OW_UIHandler.instance.searching_Icon.SetActive(true);
            OW_UIHandler.instance.searching_Icon_Parent.SetActive(false);
        }
        else
        {
            path = false;
            OW_UIHandler.instance.overview.SetActive(false);
            OW_UIHandler.instance.searching_Icon_Parent.SetActive(true);
            OW_UIHandler.instance.overview.SetActive(false);
            OW_UIHandler.instance.info_moon.SetActive(false);
            OW_UIHandler.instance.Thermometer.SetActive(false);
            LoadedModel.Clear();
        }
    }

    public void Home()
    {
        OW_UIHandler.instance.info_moon.SetActive(false);
        OW_UIHandler.instance.Ui_Panels.SetActive(true);
        OW_UIHandler.instance.overview.SetActive(false);
    }

    public void Overview()
    {
        if(detectedModel!=null)
        detectedModel.transform.GetChild(0).transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

        OW_UIHandler.instance.Ui_Panels.SetActive(false);
        OW_UIHandler.instance.overview.SetActive(true);
    }

    public void openInfo(int i)
    {
        OW_UIHandler.instance.Ui_Panels.SetActive(false);
        OW_UIHandler.instance.MoonName.text = OW_UIHandler.instance.overview.transform.GetChild(i).name;
        OW_UIHandler.instance.Descripton_Text.text = Description[i].ToString();
        OW_UIHandler.instance.info_moon.SetActive(true);
        OW_UIHandler.instance.overview.SetActive(false);
    }

    public void CloseInfo()
    {
        path = false;
        OW_UIHandler.instance.Ui_Panels.SetActive(true);
        OW_UIHandler.instance.info_moon.SetActive(false);
        OW_UIHandler.instance.overview.SetActive(true);
        LoadedModel[0].transform.localScale = Vector3.zero;
        LoadedModel[0].transform.localPosition = Vector3.zero;
        LoadedModel[0].GetComponent<OW_Rotation>().enabled = false;
        LoadedModel[0].GetComponent<LookAt>().enabled = false;
        LoadedModel[0].GetComponent<OW_MoonStart>().enabled = true;
        LoadedModel[0].transform.GetChild(0).GetComponent<MeshRenderer>().material = initmat;
        LoadedModel[1].gameObject.SetActive(false);
        LoadedModel[2].gameObject.SetActive(false);
        LoadedModel[3].gameObject.SetActive(false);
    }

    public void Revolution()
    {
        path = true;
        LoadedModel[0].GetComponent<OW_MoonStart>().enabled = false;
        LoadedModel[0].transform.GetChild(0).GetComponent<MeshRenderer>().material = mat;
        LoadedModel[0].transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        LoadedModel[0].transform.localPosition = new Vector3(-2.18f, -0.53f, 0f);
        LoadedModel[0].GetComponent<OW_Rotation>().enabled = true;
        LoadedModel[0].GetComponent<LookAt>().enabled = true;
        LoadedModel[2].gameObject.SetActive(true);
        LoadedModel[3].gameObject.SetActive(true);
    }
    public void LengthOfDay()
    {
        path = true;
        LoadedModel[0].GetComponent<OW_MoonStart>().enabled = false;
        LoadedModel[0].transform.GetChild(0).GetComponent<MeshRenderer>().material = mat;
        LoadedModel[0].transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        LoadedModel[0].transform.localPosition = new Vector3(-2.18f, -0.53f, 0f);
        LoadedModel[0].GetComponent<OW_Rotation>().enabled = true;
        LoadedModel[0].GetComponent<LookAt>().enabled = true;
        LoadedModel[1].gameObject.SetActive(true);
        LoadedModel[3].gameObject.SetActive(true);
    }
}
