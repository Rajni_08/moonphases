﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MP_UIHandler : MonoBehaviour {

    public static MP_UIHandler instance;

    public GameObject notification;
    public Text notify_text;
    public GameObject searching_Icon;
    public GameObject Phases_Btn;
    public GameObject Description_Pannel;
    public GameObject PhasesBtn;
    public GameObject HomeBtn;
    public GameObject ActivityComplete;


    // Use this for initialization
    void Awake ()
    {
        instance = this;
        //searching_Icon.SetActive(true);
    }
}
