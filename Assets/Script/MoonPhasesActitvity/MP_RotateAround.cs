﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MP_RotateAround : MonoBehaviour
{

    public Transform target;
    public float OrbitDegrees = 1f;

    void Update()
    {
        if (Moonphases.instance.path)
            transform.RotateAround(target.transform.position, Vector3.down, OrbitDegrees);
    }
}