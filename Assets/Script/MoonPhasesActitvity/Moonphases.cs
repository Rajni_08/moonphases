﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;


[System.Serializable]
public class ImageTargetInfo
{
    public string cardName;
    public GameObject imageTarget;
    public GameObject selectObject;
}



public class Moonphases : MonoBehaviour
{

    public VuforiaUnity.VuforiaHint HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS { get; private set; }

    public static Moonphases instance;

    public List<Transform> LoadedPos = new List<Transform>();
    public List<ImageTargetInfo> detectedObjectList = new List<ImageTargetInfo>();
    internal GameObject animator_obj;
    string currentObjectName;
    static int detectStoreIndex = 0;
    bool detected;
   

    void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        VuforiaUnity.SetHint(HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, 2);
    }

    private void OnEnable()
    {
        LoadModelOnDetection.isModelLoaded += OnModelLoaded;
    }


    private void OnDisable()
    {
        LoadModelOnDetection.isModelLoaded -= OnModelLoaded;
    }


    private void OnModelLoaded(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
    {
        if (status)
        {
            currentObjectName = name;
            MP_UIHandler.instance.PhasesBtn.SetActive(true);
            MP_UIHandler.instance.searching_Icon.SetActive(false);

            MP_UIHandler.instance.notification.SetActive(false);
            ImageTargetInfo imageTargetInfo = detectedObjectList.Find(item => item.cardName.ToLower() == name.ToLower());

            if (imageTargetInfo == null)
            {
                Debug.Log("in :" + imageTargetInfo);
                imageTargetInfo = new ImageTargetInfo();
                imageTargetInfo.cardName = name;
                imageTargetInfo.imageTarget = _transform.gameObject;
                imageTargetInfo.selectObject = _transform.GetChild(0).gameObject;
                detectedObjectList.Add(imageTargetInfo);
                Debug.Log(detectedObjectList.Count);
                imageTargetInfo = null;
            }

            if (name == "Activity")
            {
                Debug.Log(_transform.gameObject.transform.GetChild(0).transform.GetChild(2).gameObject);
                animator_obj = _transform.gameObject.transform.GetChild(0).transform.GetChild(2).gameObject;
                    if (_transform.gameObject.transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).GetChildCount() > 7)
                    {
                        Debug.Log("yes: " + _transform.gameObject.transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).GetChildCount());
                        for (int j = 0; j < _transform.gameObject.transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).GetChildCount(); j++)
                        {
                            //Debug.Log("for");
                            LoadedPos.Add(_transform.gameObject.transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).transform.GetChild(j));
                        }
                    }
            }    

            if (detectedObjectList.Count > 0)
            {
                for (int i = 0; i < detectedObjectList.Count; i++)
                {
                    if (detectedObjectList[i].cardName == "Activity")
                        detected = true;
                }
                if (detected)
                {
                    MP_UIHandler.instance.Description_Pannel.SetActive(true);

                    if (detectedObjectList.Count == 2)
                    IscorrectCard();  
                }
                else
                {
                    MP_UIHandler.instance.notify_text.text = "Place Activity Card";
                    MP_UIHandler.instance.notification.SetActive(true);
                    StartCoroutine(DisabeUI());
                }
            }
        }
        else
        {
            Debug.Log("notfound");
            detected = false;

            ImageTargetInfo imageTargetInfo = detectedObjectList.Find(item => item.cardName.ToLower() == name.ToLower());

            if (imageTargetInfo != null)
            {
                detectedObjectList.Remove(imageTargetInfo);
                Debug.Log(detectedObjectList.Count);

                if (imageTargetInfo.cardName == "Activity")
                {
                    MP_UIHandler.instance.Description_Pannel.SetActive(false);
                    detectStoreIndex = 0;
                    Moonphases.instance.animator_obj.GetComponent<Animator>().speed = 1;
                    MP_UIHandler.instance.notify_text.text = "Place Activity Card";
                    MP_UIHandler.instance.notification.SetActive(true);
                    LoadedPos.Clear();
                }
            }

            if (detectedObjectList.Count == 0)
            {
               MP_UIHandler.instance.PhasesBtn.SetActive(false);
               for(int i =0; i<MP_UIHandler.instance.PhasesBtn.transform.GetChildCount(); i++)
                {
                    MP_UIHandler.instance.PhasesBtn.transform.GetChild(i).gameObject.SetActive(false);
                }
                MP_UIHandler.instance.searching_Icon.SetActive(true);
                MP_UIHandler.instance.notification.SetActive(false);
                MP_UIHandler.instance.ActivityComplete.SetActive(false);
            }
        }
    }

    private bool IscorrectCard()
    {
        for (int i = 0; i < MP_UIHandler.instance.PhasesBtn.transform.childCount; i++)
        {
            int cardIndex = detectedObjectList.FindIndex(item => item.cardName == MP_UIHandler.instance.PhasesBtn.transform.GetChild(i).name);
            Debug.Log(cardIndex);
            if (cardIndex > -1)
            {
                Debug.Log("i :"+ i);
                Debug.Log("detectStoreIndex :" + detectStoreIndex);

                if (i == detectStoreIndex)
                {
                    Debug.Log("right_card");
                    Moonphases.instance.animator_obj.GetComponent<Animator>().speed = 1;
                    MP_UIHandler.instance.PhasesBtn.transform.GetChild(i).gameObject.SetActive(true);
                    MP_UIHandler.instance.PhasesBtn.transform.GetChild(i).GetComponent<Button>().interactable = true;
                    LoadedPos[i].GetComponent<Collider>().enabled = true;
                    MP_UIHandler.instance.notification.SetActive(false);
                    detectStoreIndex++;
                    return true;
                }                      
            }
        }

        MP_UIHandler.instance.notify_text.text = "Place the right card";
        MP_UIHandler.instance.notification.SetActive(true);
        StartCoroutine(DisabeUI());
        return false;
    }

    private IEnumerator DisabeUI()
    {
        yield return new WaitForSeconds(1.6f);
        MP_UIHandler.instance.notification.SetActive(false);
    }
}

