﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MP_IsTrigger : MonoBehaviour {


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("entered");
        Debug.Log("OnTriggerEnter :" + other.name);
        Moonphases.instance.animator_obj.GetComponent<Animator>().speed = 0;

        if (other.name == "Waning Crescent Moon")
        {
            StartCoroutine(completeActivity());
        }

        if (Moonphases.instance.LoadedPos.Count > 0)
        {
            for (int i = 0; i < Moonphases.instance.LoadedPos.Count; i++)
            {
                Moonphases.instance.LoadedPos[i].GetComponent<SphereCollider>().enabled = false;
            }
        }
    }

    private IEnumerator completeActivity()
    {
        yield return new WaitForSeconds(2f);
        MP_UIHandler.instance.Description_Pannel.SetActive(false);
        MP_UIHandler.instance.PhasesBtn.SetActive(false);
        Moonphases.instance.animator_obj.GetComponent<Animator>().speed = 1;
        Moonphases.instance.LoadedPos[0].GetComponent<Collider>().enabled = true;

        yield return new WaitForSeconds(2f);
        if (Moonphases.instance.LoadedPos.Count > 0)
        {
            for (int i = 0; i < Moonphases.instance.LoadedPos.Count; i++)
            {
                Moonphases.instance.LoadedPos[i].GetComponent<SphereCollider>().enabled = false;
            }
        }
        MP_UIHandler.instance.HomeBtn.SetActive(true);
        MP_UIHandler.instance.ActivityComplete.SetActive(true);
        transform.GetComponent<TouchActivity>().enabled = true;

        yield return new WaitForSeconds(1f);
        Moonphases.instance.animator_obj.GetComponent<Animator>().speed = 1;
    }
}
