﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenemanager : MonoBehaviour {

    public void Activity()
    {
        SceneManager.LoadScene("Activity");
    }

    public void Visualization()
    {
        SceneManager.LoadScene("PhasesOfMoon");
    }

    public void Overview()
    {
        SceneManager.LoadScene("OverviewOfMoon");
    }
}
