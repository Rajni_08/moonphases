﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MAIP_IsTrigger : MonoBehaviour {


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("entered");
        Debug.Log(other.name);
        Moon_script.instance.path = false;
        
        if(Moon_script.instance.LoadedPos.Count >0 )
        {
            for (int i = 0; i < Moon_script.instance.LoadedPos.Count; i++)
            {
                Moon_script.instance.LoadedPos[i].GetComponent<SphereCollider>().enabled = false;
            }
            for (int i = 0; i < MAIP_UIHandler.instance.PhasesBtn.transform.GetChildCount(); i++)
            {
                MAIP_UIHandler.instance.PhasesBtn.transform.GetChild(i).GetComponent<Button>().interactable = true;
            }
            MAIP_UIHandler.instance.Home_Btn.SetActive(false);
            MAIP_UIHandler.instance.Description_Pannel.SetActive(true);
            MAIP_UIHandler.instance.DescriptionCrossBtn.GetComponent<Button>().interactable = true;
            MAIP_UIHandler.instance.ViewFromEarth_Btn.SetActive(true);
        }
    }
}
