﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MAIP_Rotation : MonoBehaviour {

    public static MAIP_Rotation instance;

    internal GameObject currentModel;

    public float speed = 0.1f;

    void Awake()
    {
        instance = this;    
    }

    // Update is called once per frame
    void Update ()
    {
        if (currentModel != null)
        {
            //Debug.Log("Update");
            currentModel.transform.Rotate(Vector3.up * Time.deltaTime * speed, Space.World);
        }
    }
}
