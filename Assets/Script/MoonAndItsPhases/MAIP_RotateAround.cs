﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MAIP_RotateAround : MonoBehaviour
{

    public Transform target;
    public float OrbitDegrees = 1f;

    void Update()
    {
        if (Moon_script.instance.path)
        {
            transform.RotateAround(target.position, Vector3.down, OrbitDegrees);
            //Debug.Log("Path_True");
        }
    }
}