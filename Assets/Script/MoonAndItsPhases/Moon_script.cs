﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class Moon_script : MonoBehaviour {

    public VuforiaUnity.VuforiaHint HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS { get; private set; }

    public static Moon_script instance;
    public List<GameObject> LoadedModel;
    public List<Transform> LoadedPos;
    public List<GameObject> detectedModel;
    public List<String> MoonDescription;
    public float OrbitDegrees = 1f;
    private float timeCount = 0.0f;
    internal bool path;
    internal bool Isrotate;
    internal Transform target;
    Vector3 initPos;
    Quaternion initRot;
    bool PhasesOfMoon;
    float initialminZoom, initialmaxZoom;


    void Awake()
    {
       instance = this;
        initialminZoom = MAIP_ZoomInOut_Script.instance.minZoom;
        initialmaxZoom = MAIP_ZoomInOut_Script.instance.maxZoom;
    }

    private void Start()
    {
        VuforiaUnity.SetHint(HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, 1);
    }

    private void Update()
    {
        if (Isrotate)
        {
            if (target.name == "New Moon")
            { 
                Debug.Log("NewMoon"); 
                detectedModel[0].transform.rotation = Quaternion.Slerp(Quaternion.Euler(0, 0, 0), Quaternion.Euler(0, 90, 0), timeCount * 0.5f);
                timeCount = timeCount + Time.deltaTime;
            }
            else if (target.name == "Waxing Crescent Moon")
            {
                Debug.Log("WaxingCrescentMoon");
                detectedModel[0].transform.rotation = Quaternion.Slerp(Quaternion.Euler(0, 0, 0), Quaternion.Euler(0, 130, 0), timeCount * 0.5f);
                timeCount = timeCount + Time.deltaTime;
            }
            else if (target.name == "First Quarter Moon")
            {
                Debug.Log("FirstQuarterMoon");
                detectedModel[0].transform.rotation = Quaternion.Slerp(Quaternion.Euler(0, 0, 0), Quaternion.Euler(0, 180, 0), timeCount * 0.5f);
                timeCount = timeCount + Time.deltaTime;
            }
            else if (target.name == "Waxing Gibbous Moon")
            {
                Debug.Log("WaxingGibbousMoon");
                detectedModel[0].transform.rotation = Quaternion.Slerp(Quaternion.Euler(0, 0, 0), Quaternion.Euler(0, 210, 0), timeCount * 0.5f);
                timeCount = timeCount + Time.deltaTime;
            }
            else if (target.name == "Full Moon")
            {
                Debug.Log("FullMoon");
                detectedModel[0].transform.rotation = Quaternion.Slerp(Quaternion.Euler(0, 0, 0), Quaternion.Euler(0, -90, 0), timeCount * 0.5f);
                timeCount = timeCount + Time.deltaTime;
            }
            else if (target.name == "Waning Gibbous Moon")
            {
                Debug.Log("WaningGibbousMoon ");
                detectedModel[0].transform.rotation = Quaternion.Slerp(Quaternion.Euler(0, 0, 0), Quaternion.Euler(0, -50, 0), timeCount * 0.5f);
                timeCount = timeCount + Time.deltaTime;
            }
            else if (target.name == "Third Quarter Moon")
            {
                Debug.Log("ThirdQuarterMoon");
                //detectedModel[0].transform.rotation = Quaternion.Slerp(Quaternion.Euler(0, 0, 0), Quaternion.Euler(0, -90, 0), timeCount);
                //timeCount = timeCount + Time.deltaTime;
            }
            else if (target.name == "Waning Crescent Moon")
            {
                Debug.Log("WaningCrescentMoon ");
                detectedModel[0].transform.rotation = Quaternion.Slerp(Quaternion.Euler(0, 0, 0), Quaternion.Euler(0, 50, 0), timeCount * 0.5f);
                timeCount = timeCount + Time.deltaTime;
            }
        }
    }

    private void OnEnable()
    {    
        LoadModelOnDetection.isModelLoaded += OnModelLoaded;
        TrackableEventHandler.onImageTargetDetected += TrackableEventHandler_OnImageTargetDetected;
    }

    private void TrackableEventHandler_OnImageTargetDetected(string arg, Transform _transform, bool isFound)
    {
        if(!isFound)
        {
            initPos = _transform.position;
            initRot = _transform.rotation;
        }
           
    }

    private void OnDisable()
    {
        LoadModelOnDetection.isModelLoaded -= OnModelLoaded;
    }


    private void OnModelLoaded(string name, Transform _transform, bool status, LoadModelOnDetection.SceneType sceneType)
    {
        MAIP_ZoomInOut_Script.instance.isModlDetected = status;
        Debug.Log("Name:" + name);

        if (status)
        {
            MAIP_UIHandler.instance.First_inst.SetActive(false);
            MAIP_UIHandler.instance.searching_Icon.SetActive(false);
            MAIP_UIHandler.instance.PhasesOfMoon_Btn.SetActive(true);
            MAIP_UIHandler.instance.info_Btn.SetActive(true);
            MAIP_UIHandler.instance.Home_Btn.SetActive(true);
            detectedModel.Add(_transform.GetChild(0).gameObject);

            if (name == "Moon")
            {
                if (_transform.gameObject.transform.GetChildCount() > 0)
                {
                    Debug.Log(_transform.gameObject.transform.GetChildCount());

                    for (int i = 0; i < _transform.gameObject.transform.GetChild(0).transform.GetChildCount(); i++)
                    {
                        if (_transform.gameObject.transform.GetChild(0).transform.GetChild(i).transform.gameObject.activeInHierarchy
                            && _transform.gameObject.transform.GetChild(0).transform.GetChild(i).transform.gameObject.name == "Moon")
                        {
                            Debug.Log(_transform.gameObject.transform.GetChild(0).transform.GetChild(i).transform.gameObject.name);
                            MAIP_Rotation.instance.currentModel = _transform.gameObject.transform.GetChild(0).transform.GetChild(i).transform.gameObject;
                            MAIP_ZoomInOut_Script.instance.currModel = _transform.gameObject.transform.GetChild(0).transform.GetChild(i).transform.gameObject;
                        }
                            LoadedModel.Add(_transform.gameObject.transform.GetChild(0).transform.GetChild(i).transform.gameObject);

                        Debug.Log(_transform.gameObject.transform.GetChild(0).transform.GetChild(i).gameObject.name);
                        if (_transform.gameObject.transform.GetChild(0).transform.GetChild(i).transform.GetChildCount() > 7)
                        {
                            Debug.Log("yes: "+ _transform.gameObject.transform.GetChild(0).transform.GetChild(i).transform.GetChildCount());
                            for (int j = 0; j < _transform.gameObject.transform.GetChild(0).transform.GetChild(i).transform.GetChildCount(); j++)
                            {
                                Debug.Log("for");
                                LoadedPos.Add(_transform.gameObject.transform.GetChild(0).transform.GetChild(i).transform.GetChild(j));
                            }
                        }
                    }
                }
            }
        }
        else
        {
            MAIP_UIHandler.instance.First_inst.SetActive(true);
            MAIP_UIHandler.instance.Home_Btn.SetActive(false);
            MAIP_UIHandler.instance.PhasesOfMoon_Btn.SetActive(false);
            MAIP_UIHandler.instance.info_Btn.SetActive(false);
            MAIP_UIHandler.instance.info_moon.SetActive(false);
            MAIP_UIHandler.instance.searching_Icon.SetActive(true);
            MAIP_UIHandler.instance.Phases_Btn.SetActive(false);
            MAIP_UIHandler.instance.Description_Pannel.SetActive(false);
            MAIP_UIHandler.instance.ViewFromEarth_Btn.SetActive(false);
            MAIP_UIHandler.instance.NormalView_Btn.SetActive(false);
            MAIP_UIHandler.instance.Phases_Animation.SetActive(false);
            MAIP_UIHandler.instance.info_Btn.GetComponent<Button>().interactable = true;
            MAIP_ZoomInOut_Script.instance.minZoom = initialminZoom;
            MAIP_ZoomInOut_Script.instance.maxZoom = initialmaxZoom;

            if (MAIP_Rotation.instance.currentModel!=null)
                MAIP_Rotation.instance.currentModel = null;

            for (int i = 0; i < MAIP_UIHandler.instance.PhasesBtn.transform.GetChildCount(); i++)
            {
                MAIP_UIHandler.instance.PhasesBtn.transform.GetChild(i).GetComponent<Button>().interactable = true;
            }

            path = false;
            detectedModel.Clear();
            LoadedModel.Clear();
            LoadedPos.Clear();
            MAIP_UIHandler.instance.ReverseRotation_Btn.SetActive(false);
        }
    }

    public void PhaseOfMoon(Material mat)
    {
        Debug.Log("PhaseOfMoon");
        Debug.Log(LoadedModel[0].transform.parent.gameObject);     
        LoadedModel[0].transform.localPosition = new Vector3(-0.328f, 0.15f, 0.004f);
        LoadedModel[0].transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        LoadedModel[0].transform.localRotation = Quaternion.EulerRotation(-8f, -90f, 0f);
        LoadedModel[0].GetComponent<LookAt>().enabled = true;
        LoadedModel[0].transform.GetChild(0).GetComponent<MeshRenderer>().material = mat;
        MAIP_ZoomInOut_Script.instance.minZoom = 1f;
        MAIP_ZoomInOut_Script.instance.maxZoom = 1.5f;
        MAIP_ZoomInOut_Script.instance.currModel = LoadedModel[0].transform.parent.gameObject;

        for (int i = 0; i < LoadedModel.Count; i++)
        {
            if (!LoadedModel[i].gameObject.activeInHierarchy && LoadedModel[i].gameObject.name != "ReversePath")
                LoadedModel[i].gameObject.SetActive(true);
        }

        path = true;
        MAIP_Rotation.instance.currentModel = null;
        MAIP_UIHandler.instance.ReverseRotation_Btn.SetActive(true);
    }

    public void OnClick_Pos(int index)
    {
        MAIP_UIHandler.instance.DescriptionCrossBtn.GetComponent<Button>().interactable = false;

        for (int i = 0; i < MAIP_UIHandler.instance.PhasesBtn.transform.GetChildCount(); i++)
        {
            MAIP_UIHandler.instance.PhasesBtn.transform.GetChild(i).GetComponent<Button>().interactable = false; 
        }

        if (LoadedPos.Count > 0)
        {
            if(target!=null)
            if (target.name != LoadedPos[index].name)
                path = true;

            target = LoadedPos[index];
            Debug.Log("target:" + target);

            target.GetComponent<SphereCollider>().enabled = true;

            MAIP_UIHandler.instance.MoonName.text = target.name;
            MAIP_UIHandler.instance.Descripton_Text.text = MoonDescription[index];
            MAIP_UIHandler.instance.Home_Btn.SetActive(true);
            MAIP_UIHandler.instance.Description_Pannel.SetActive(false);
        }
    }

    public void ClearDescription()
    {
        for (int i = 0; i < MAIP_UIHandler.instance.PhasesBtn.transform.GetChildCount(); i++)
        {
            MAIP_UIHandler.instance.PhasesBtn.transform.GetChild(i).GetComponent<Button>().interactable = true;
        }
    }

    public void ViewFromEarth()
    {
        MAIP_UIHandler.instance.PhasesBtn.SetActive(false);
        Isrotate = true;        
    }

    public void NormalView()
    {
        timeCount = 0.0f;
        Isrotate = false;
        path = true;
        MAIP_UIHandler.instance.PhasesBtn.SetActive(true);
        MAIP_UIHandler.instance.Description_Pannel.SetActive(false);
        detectedModel[0].transform.position = initPos;
        detectedModel[0].transform.rotation = initRot;
    }

    public void ReverseRotation()
    {
        Isrotate = false;
        detectedModel[0].transform.position = initPos;
        detectedModel[0].transform.rotation = initRot;
        MAIP_UIHandler.instance.PhasesBtn.SetActive(false);
        MAIP_UIHandler.instance.Description_Pannel.SetActive(false);

        for (int i = 0; i < LoadedModel.Count; i++)
        {
            if (LoadedModel[i].name == "Moon" || LoadedModel[i].name == "Path")
                LoadedModel[i].SetActive(false);
            else
                //if(!(LoadedModel[i].name == "Sun"))
                LoadedModel[i].SetActive(true);
        }
    }
}
